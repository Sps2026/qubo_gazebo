# Onboarding Task Instructions:

NOTE: In the R@M development environment, this task is located in the `~/qubo/src/qubo_gazebo/onboarding_task/` directory.

## Task:

In our simulation and on the real robot, we want to accurately and autonomously control the depth of the robot in the pool. The robot is passively self-righting, so we are only going to control our vertical, z axis thrust. You can do binary constant control, p control, PID control, or something else! We can compare our solutions by examining the desired vs current depth plot produced by the depthSetter program.

## Getting the latest code

Make sure you have the latest code from this repository by running the following commands:

```sh
cd ~/qubo/src/qubo_gazebo
git pull
```

## Setting up dependencies

Dependencies:

- opencv (trackbar handling/window creation, weird I know)
- matplotlib (lineplot)
- moviepy (for writing matplotlib plots to opencv/numpy images)
- rospy (ros)
- numpy
- everything else required to run simulation

Most of these will be installed already on the VM dev-env.

You can install the rest with:

```sh
python2.7 -m pip install moviepy matplotlib
```

## To run/test:

To open the simulation with qubo in the pool, run `roslaunch qubo_gazebo nbrfQubo.launch`.

To run the depth setter, which updates desired depth and curr depth(converts pressure to depth) run `python desiredDepthSetter.py`.

To run your controller, run `python depthController.py`.

All of these files are contained in the qubo_gazebo/onboarding_task folder

There is a launch file which will launch the simulator, the desired depth setter, and the depth controller. This can be run by running `roslaunch qubo_gazebo onboardTask.launch`.


## Important ROS Topics:

Desired Depth is published to /desiredDepth, it is a float32 message

Current Depth is publish to /currDepth, its is a float32 message

The topic you must publish to in order to accelerate qubo is:

/qubo_gazebo/thruster_manager/input, it is a Wrench message

Documentation for all of these message types can be found on the ros std msg documentation online.


The general idea is you must subscribe to desired and current depth, figure out how you should react according to that, and write that thrust to the thrust topic. Your wrench message should be all 0 except for the linear z axis. Keep in mind that the simulation is not tuned to our actual robot perfectly, and there are no thrust limits, so you can send qubo into space if you write a high enough thrust. Making the simulation more realistic is an overdue software task some of yall could take on later this semester!

This is not a test/exam/requirement, it's offered to help you learn. Try to finish the task without looking at any of the boilerPlate or solved files, which are also in the onboarding task folder. You should be able to figure out everything you need by asking questions in-person / on slack. The ROS intro tutorials are very helpful for figuring out ros publisher/subscriber boilerplate. We expect this to take about 2 weeks, so please do not review the partially completed solution until after the first week. Learning the boilerplate is likely the biggest learning curve actively contributing to high-level software for the robot. Another very helpful tool for debugging the system is the rostopic, rosnode, commands, as well as the whole ros terminal toolset.
