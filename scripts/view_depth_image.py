import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

bridge = CvBridge()
def view_callback(data):
    cv_img = bridge.imgmsg_to_cv2(data, desired_encoding = "passthrough")
    #cv_img = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
    cv2.imshow("Depth Image", cv_img)
    cv2.waitKey(30)

def viewer():
    rospy.init_node("image_viewer", anonymous = False)

    rospy.Subscriber("/qubo_gazebo/depth_image", Image, view_callback)

    rospy.spin()

if __name__ == "__main__":
    viewer()
