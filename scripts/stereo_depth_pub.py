#!/usr/bin/env python
import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

class DepthPub:

    def left_callback(self, data):
        self.left_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
        self.left_avail = True
        if self.right_avail:
            self.pub_depth_image()
    def right_callback(self, data):
        self.right_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
        self.right_avail = True
        if self.left_avail:
            self.pub_depth_image()

    def get_depth_image(self, left, right):
        disparity = self.stereo.compute(left, right)
        return disparity

    def pub_depth_image(self):
        left = cv2.cvtColor(self.left_image, cv2.COLOR_BGR2GRAY)
        right = cv2.cvtColor(self.right_image, cv2.COLOR_BGR2GRAY)
        self.left_avail = False
        self.right_avail = False

        self.depth_image = self.get_depth_image(left, right).astype("uint8")


        #below lines for testing
        stackedImg = cv2.hconcat([left, right])
        stackedImg = cv2.resize(stackedImg, (stackedImg.shape[1]/2, stackedImg.shape[0]/2))
        stackedImg = cv2.vconcat([stackedImg, self.depth_image])

        #for non-testing, dont publish the stacked image
        self.img_msg = self.bridge.cv2_to_imgmsg(stackedImg, encoding="passthrough")

        #self.img_msg = self.bridge.cv2_to_imgmsg(self.depth_image, encoding="passthrough")
        self.depth_pub.publish(self.img_msg)

        
    def __init__(self, left_topic, right_topic, depth_topic):
        rospy.init_node('depth_pub', anonymous = True)
        
        self.depth_pub = rospy.Publisher(depth_topic, Image, queue_size = 2)
        left_sub = rospy.Subscriber(left_topic, Image, self.left_callback)
        right_sub = rospy.Subscriber(right_topic, Image, self.right_callback)

        self.stereo = cv2.StereoBM_create(numDisparities=16, blockSize=21)
        self.left_avail = False
        self.right_avail = False

        self.bridge = CvBridge()

        rospy.spin()

if __name__ == "__main__":
    try:
        left_topic = "/qubo_gazebo/qubo_gazebo/cameraleft/camera_image"
        right_topic = "/qubo_gazebo/qubo_gazebo/cameraright/camera_image"
        depth_topic = "/qubo_gazebo/depth_image"
        depth_pub = DepthPub(left_topic, right_topic, depth_topic)
    except rospy.ROSInterruptException:
        pass
