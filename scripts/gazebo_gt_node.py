#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray, MultiArrayLayout, MultiArrayDimension

QUEUE_SIZE = 2
ROSPY_RATE_HZ = 2

def get_gate_pose_gt():
    # TODO: load positions dynamically instead of hardcoding.
    initial_gate_position = np.array([0, -0.8, -1.5])
    initial_robot_position = np.array([0, -5.0, 0])

    t_vec = initial_gate_position - initial_robot_position
    r_vec = [0, 0, 0]

    return t_vec, r_vec


def gazebo_gt_pub(gate_pose_gt_topic):
    rospy.init_node('gt_pub', anonymous=True)
    gate_pose_gt_publisher = rospy.Publisher(gate_pose_gt_topic, Float64MultiArray, queue_size=QUEUE_SIZE)
    rate = rospy.Rate(ROSPY_RATE_HZ)

    while not rospy.is_shutdown():
        t_vec, r_vec = get_gate_pose_gt()

        gate_pose_gt_msg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(
                t_vec[0], t_vec[1], t_vec[2],
                r_vec[0], r_vec[1], r_vec[2]
            )
        )
        gate_pose_gt_publisher.publish(gate_pose_gt_msg)
        rate.sleep()

if __name__ == "__main__":
    try:
        gate_pose_gt_topic = "/qubo_gazebo/gate_pose_gt"
        gazebo_gt_pub(gate_pose_gt_topic=gate_pose_gt_topic)
    except rospy.ROSInterruptException:
        pass